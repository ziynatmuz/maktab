<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

<!-- Sidebar - Brand -->
<a class="sidebar-brand d-flex align-items-center justify-content-center" href="/">
    <div class="sidebar-brand-icon rotate-n-15">
        <i class="fas fa-laugh-wink"></i>
    </div>
    <div class="sidebar-brand-text mx-3">SB Admin <sup>2</sup></div>
</a>

<!-- Divider -->
<hr class="sidebar-divider my-0">

<!-- Nav Item - Dashboard -->
<li class="nav-item active">
    <a class="nav-link" href="/">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span></a>
</li>
<li class="nav-item active">
    <a class="nav-link" href="/pages/teachers/index.php">
        <i class="fas fa-chalkboard-teacher"></i>
        <span>Teachers</span></a>
</li>
<li class="nav-item active">
    <a class="nav-link" href="/pages/students/index.php">
    <i class="fas fa-user-graduate"></i>
        <span>Students</span></a>
</li>
<li class="nav-item active">
    <a class="nav-link" href="/pages/classes/index.php">
    <i class="fab fa-odnoklassniki-square"></i>
        <span>Classes</span></a>
</li>
<li class="nav-item active">
    <a class="nav-link" href="/pages/subject/index.php">
    <i class="fas fa-book"></i>
        <span>Subject</span></a>
</li>
<li class="nav-item active">
    <a class="nav-link" href="/pages/teachers_salary/index.php">
        <i class="fas fa-comment-dollar"></i>
        <span>Teachers Salary</span></a>
</li>
<li class="nav-item active">
    <a class="nav-link" href="/pages/marks/index.php">
    <i class="fas fa-address-card"></i>
        <span>Marks</span></a>
</li>
<li class="nav-item active">
    <a class="nav-link" href="/pages/rooms/index.php">
    <i class="fas fa-person-booth"></i>
        <span>Rooms</span></a>
</li>
<li class="nav-item active">
    <a class="nav-link" href="/pages/students_phone/index.php">
    <i class="fas fa-phone"></i>
        <span>Students Phone</span></a>
</li>
<!-- Divider -->
<hr class="sidebar-divider">
</ul>   