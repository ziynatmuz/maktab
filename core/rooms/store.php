<?php
$errors = [];
if ($_SERVER['REQUEST_METHOD'] ==   'POST') {

	require_once($_SERVER['DOCUMENT_ROOT'] . '/core/database.php');
	if (array_key_exists('rooms_name', $_POST) && !empty($_POST['rooms_name'])) {
		$rooms_name = $_POST['rooms_name'];
	} else {
		$errors['rooms_name'] = 'Rooms Name is required!';
	}
	if (array_key_exists('rooms_flour', $_POST) && !empty($_POST['rooms_flour'])) {
		$rooms_flour = $_POST['rooms_flour'];
	} else {
		$errors['rooms_flour'] = 'Rooms Flour is required!';
	}

	if (count($errors) == 0) {
		$sql = "INSERT INTO rooms values(null,'$rooms_name','$rooms_flour')";
		$status = mysqli_query($conn, $sql);
		if ($status) {
			header('Location: /pages/rooms/index.php');
		} else {
			dump($status);
		}
	} else {
		dump($errors);
	}
}
