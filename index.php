<!DOCTYPE html>
<html lang="en">

<?php require_once('components/header.php'); ?>

<body id="page-top">

	<!-- Page Wrapper -->
	<div id="wrapper">

		<!-- Sidebar -->
		<?php require_once('components/left_menu.php'); ?>
		<!-- End of Sidebar -->

		<!-- Content Wrapper -->
		<div id="content-wrapper" class="d-flex flex-column">

			<?php require_once('components/top_menu.php'); ?>
			<!-- Main Content -->

			<?php require_once('components/main.php'); ?>


			<?php require_once('components/footer.php'); ?>
			<?php require_once('components/scripts.php'); ?>
</body>

</html>