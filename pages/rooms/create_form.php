<h1 class="my-4">Create Room</h1>
<form action="/core/rooms/store.php" method="POST">
	<div class="mb-3">
		<!-- rooms_name -->
		<label for="rooms_name" class="form-label">Rooms_name :</label>
		<input type="text" class="form-control" id="rooms_name" name="rooms_name">
	</div>
	<div class="mb-3">
		<!-- rooms_flour -->
		<label for="rooms_flour" class="form-label">Rooms_flour :</label>
		<input type="text" class="form-control" id="rooms_flour" name="rooms_flour">
	</div>
	<button type="submit" class="btn btn-primary">Submit</button>
</form>