<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/core/database.php');
$sql = 'SELECT * FROM rooms';
$data = mysqli_query($conn, $sql);

?>
<h1>Rooms Table</h1>
<table class="table ">
	<a type="button" class="btn btn-primary mx-auto m-4 w-25" href="/pages/rooms/create.php">Add Room+</a>
	<thead>
		<tr>
			<th scope="col">Rooms_name</th>
			<th scope="col">Rooms_flour</th>
			<th scope="col ">Buttons</th>
		</tr>
	</thead>
	<tbody>
		<?php if (mysqli_num_rows($data) > 0) : ?>
			<?php while ($room = mysqli_fetch_assoc($data)) : ?>
				<tr>
					<td scope="row"><?= $room['rooms_name'] ?></td>
					<td><?= $room['rooms_flour'] ?></td>
					<td>
						<a type="button" class="btn btn-warning" href="/pages/rooms/update.php?id=<?= $room['id'] ?>">Update</a>
						<a type="button" class="btn btn-success" href="/pages/rooms/show.php?id=<?= $room['id'] ?>">Show</a>
						<a type="button" class="btn btn-danger" href="/core/rooms/delete.php?id=<?= $room['id'] ?>">Delete</a>
					</td>
				</tr>
			<?php endwhile ?>
		<?php endif ?>
	</tbody>
</table>