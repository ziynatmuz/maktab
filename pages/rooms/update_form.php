<?php
if (array_key_exists('id', $_GET) && !empty($_GET['id'])) {
	$id = $_GET['id'];
	require_once($_SERVER['DOCUMENT_ROOT'] . '/core/database.php');
	$sql = "SELECT * FROM rooms WHERE id=$id";
	$status = mysqli_query($conn, $sql);
	if (mysqli_num_rows($status) > 0) {
		$room = mysqli_fetch_assoc($status);
	} else {
		dump("$id li room no!");
	}
}

?>

<h1 class="my-4">Update Rooms</h1>
<form action="/core/rooms/update.php" method="POST">
	<div class="mb-3">
		<!-- rooms_name -->
		<label for="rooms_name" class="form-label">Rooms_name :</label>
		<input type="text" class="form-control" id="rooms_name" name="rooms_name" value="<?= $room['rooms_name'] ?>">
	</div>
	<div class="mb-3">
		<!-- rooms_flour -->
		<label for="rooms_flour" class="form-label">Rooms_flour :</label>
		<input type="text" class="form-control" id="rooms_flour" name="rooms_flour" value="<?= $room['rooms_flour'] ?>">
	</div>
	<input type="hidden" class="form-control" name="id" value="<?= $room['id'] ?>">
	</div>
	<button type="submit" class="btn btn-primary">Update</button>
</form>