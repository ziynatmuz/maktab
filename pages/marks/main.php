<?php
require_once ($_SERVER['DOCUMENT_ROOT'].'/core/database.php');
$sql = 'SELECT * FROM marks';
$data = mysqli_query($conn,$sql);

?>
<h1>Marks  Table</h1>
<table class="table ">
<a type="button" class="btn btn-primary mx-auto m-4 w-25" href="/pages/marks/create.php">Add Mark+</a>
  <thead>
    <tr>
      <th scope="col">Id</th>
      <th scope="col">Student Name</th>
      <th scope="col">Mark</th>
      <th scope="col">Subject Name</th>
      <th scope="col ">Buttons</th>
    </tr>
  </thead>
  <tbody>
      <?php if(mysqli_num_rows($data) > 0) : ?>
      <?php while($mark = mysqli_fetch_assoc($data) ) : ?>
    <tr>
      <td scope="row"><?= $mark['id']?></td>
      <td><?= $mark['student_name'] ?></td>
      <td><?= $mark['mark'] ?></td>
      <td><?= $mark['subject_name'] ?></td>
      <td>
          <a type="button" class="btn btn-warning" href="/pages/marks/update.php?id=<?= $mark['id'] ?>">Update</a>
          <a type="button" class="btn btn-success" href="/pages/marks/show.php?id=<?= $mark['id'] ?>">Show</a>
          <a type="button" class="btn btn-danger" href="/core/marks/delete.php?id=<?= $mark['id'] ?>">Delete</a>
      </td>
    </tr>
    <?php endwhile ?>
    <?php endif ?>
  </tbody>
</table>