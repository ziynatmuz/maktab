<?php 
require_once($_SERVER['DOCUMENT_ROOT'].'/core/database.php');
$sql2 = "SELECT * FROM subjects";
$table_data2 = mysqli_query($conn, $sql2);
if (mysqli_num_rows($table_data2) > 0) {
    $subjects = [];
    while($subject = mysqli_fetch_assoc($table_data2)) {
        $subjects[] = $subject;
    }
}
$sql2 = "SELECT * FROM students";
$table_data2 = mysqli_query($conn, $sql2);
if (mysqli_num_rows($table_data2) > 0) {
    $students = [];
    while($student = mysqli_fetch_assoc($table_data2)) {
        $students[] = $student;
    }
}
?>

<h1 class="my-4">Create Mark</h1>
<form action="/core/marks/store.php" method="POST">
  <div class="form-group mb-3">
        <label for="exampleFormControlSelect1">SELECT STUDENT :</label>
        <select name='student_name' class="form-control" id="exampleFormControlSelect1">
            <?php if (count($students) > 0) : ?>
                <?php foreach ($students as $student) : ?>
                    <option value="<?= $student['name'] ?>"><?= $student['name']?></option>
                <?php endforeach; ?>
            <?php endif; ?>
        </select>
    </div>
  
  <div class="mb-3">
    <label  class="form-label">Mark :</label>
    <input type="text" class="form-control"  name="mark">
  </div>
  <div class="form-group mb-3">
        <label for="exampleFormControlSelect1">SELECT SUBJECT :</label>
        <select name='subject_name' class="form-control" id="exampleFormControlSelect1">
            <?php if (count($subjects) > 0) : ?>
                <?php foreach ($subjects as $subject) : ?>
                    <option value="<?= $subject['name'] ?>"><?= $subject['name']?></option>
                <?php endforeach; ?>
            <?php endif; ?>
        </select>
    </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>