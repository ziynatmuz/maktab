<?php
if(array_key_exists('id',$_GET) && !empty($_GET['id'])){
$id = $_GET['id'];
require_once ($_SERVER['DOCUMENT_ROOT'].'/core/database.php');
$sql = "SELECT * FROM marks WHERE id=$id";
$status = mysqli_query($conn,$sql);
if(mysqli_num_rows($status) > 0){
    $mark = mysqli_fetch_assoc($status);
}else{
    dump("$id li mark yoq");
}
}

?>

<h1 class="my-4">Update Mark</h1>
<form action="/core/marks/update.php" method="POST">
  
  <div class="mb-3">
    <label  class="form-label">Student Name :</label>
    <input type="text" class="form-control" name="student_name" value="<?= $mark['student_name']?>">
  </div>
  <div class="mb-3">
    <label for="subject" class="form-label">Mark</label>
    <input type="text" class="form-control"  name="mark" value="<?= $mark['mark']?>">
    <input type="hidden" class="form-control" name="id" value="<?= $mark['id']?>">
  </div>
  <div class="mb-3">
    <label  class="form-label">Subject Name :</label>
    <input type="text" class="form-control" name="subject_name" value="<?= $mark['subject_name']?>">
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>