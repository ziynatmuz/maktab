<!DOCTYPE html>
<html lang="en">
<?php require_once($_SERVER['DOCUMENT_ROOT'].'/components/header.php'); ?>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <?php require_once($_SERVER['DOCUMENT_ROOT'].'/components/left_menu.php'); ?>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <?php require_once($_SERVER['DOCUMENT_ROOT'].'/components/top_menu.php'); ?>
            <!-- Main Content -->
            <div class="container">
            <?php require_once($_SERVER['DOCUMENT_ROOT'].'/pages/teachers_salary/create_form.php'); ?>
            </div>
            <!-- End MAin php -->
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/components/footer.php'); ?>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/components/scripts.php'); ?>
    </body>
    
</html>