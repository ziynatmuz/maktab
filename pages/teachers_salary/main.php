<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/core/database.php');
$sql3 = "SELECT * FROM teachers_salary ";
$data3 = mysqli_query($conn, $sql3);
?>
<h1>Teachers Salary</h1>
<table class="table ">
  <a type="button" class="btn btn-primary mx-auto m-4 w-25" href="/pages/teachers_salary/create.php">Add Teacher Salary+</a>
  <thead>
    <tr>
      <th scope="col">Teacher</th>
      <th scope="col">Subject</th>
      <th scope="col">Price</th>
      <th scope="col ">Buttons</th>
    </tr>
  </thead>
  <tbody>
    <?php if (mysqli_num_rows($data3) > 0) : ?>
      <?php while ($row = mysqli_fetch_assoc($data3)) : ?>
       
        <tr>
          <td><?= $row['teachers_name'] ?></td>
          <td><?= $row['subject_name'] ?></td>
          <td><?= $row['price'] ?></td>
          <td>
            <a type="button" class="btn btn-warning" href="/pages/teachers_salary/update.php?id=<?= $row['id'] ?>">Update</a>
            <a type="button" class="btn btn-success" href="/pages/teachers_salary/show.php?id=<?= $row['id'] ?>">Show</a>
            <a type="button" class="btn btn-danger" href="/core/teachers_salary/delete.php?id=<?= $row['id'] ?>">Delete</a>
          </td>
        </tr>

      <?php endwhile ?>
    <?php endif ?>
  </tbody>
</table>