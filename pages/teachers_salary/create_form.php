<?php 
require_once($_SERVER['DOCUMENT_ROOT'].'/core/database.php');
$sql = "SELECT * FROM ustozlar";
$table_data = mysqli_query($conn, $sql);
if (mysqli_num_rows($table_data) > 0) {
    $ustozlar = [];
    while($ustoz = mysqli_fetch_assoc($table_data)) {
        $ustozlar[] = $ustoz;
    }
}

$sql2 = "SELECT * FROM subjects";
$table_data2 = mysqli_query($conn, $sql2);
if (mysqli_num_rows($table_data2) > 0) {
    $subjects = [];
    while($subject = mysqli_fetch_assoc($table_data2)) {
        $subjects[] = $subject;
    }
}

?>

<h1 class="my-4">Create Teacher Salary</h1>
<form action="/core/teachers_salary/store.php" method="POST">

  <div class="form-group mb-3">
        <label for="exampleFormControlSelect1">SELECT TEACHER :</label>
        <select name='teachers_name' class="form-control" id="exampleFormControlSelect1">
            <?php if (count($ustozlar) > 0) : ?>
                <?php foreach ($ustozlar as $ustoz) : ?>
                    <option value="<?= $ustoz['name'] ?>"><?= $ustoz['name']?></option>
                <?php endforeach; ?>
            <?php endif; ?>
        </select>
    </div>
  
    <div class="form-group mb-3">
        <label for="exampleFormControlSelect1">SELECT SUBJECT :</label>
        <select name='subject_name' class="form-control" id="exampleFormControlSelect1">
            <?php if (count($subjects) > 0) : ?>
                <?php foreach ($subjects as $subject) : ?>
                    <option value="<?= $subject['name'] ?>"><?= $subject['name']?></option>
                <?php endforeach; ?>
            <?php endif; ?>
        </select>
    </div>

    <div class="form-group mb-3">
        <label for="exampleFormControlSelect1">SELECT PRICE:</label>
        <select name='price' class="form-control" id="exampleFormControlSelect1">
          <option value="$100"><h1>$100</h1></option>
          <option value="$250"><h1>$250</h1></option>
          <option value="$400"><h1>$400</h1></option>
          <option value="$550"><h1>$550</h1></option>
          <option value="$600"><h1>$600</h1></option>
          <option value="$750"><h1>$750</h1></option>
          <option value="$1000"><h1>$1000</h1></option>
        </select>
    </div>
  
  <button type="submit" class="btn btn-primary">Submit</button>
</form>