<?php
require_once ($_SERVER['DOCUMENT_ROOT'].'/core/database.php');
$sql = 'SELECT * FROM students_phone';
$data = mysqli_query($conn,$sql);

?>
<h1>Students Phone Number</h1>
<table class="table ">
<a type="button" class="btn btn-primary mx-auto m-4 w-25" href="/pages/students_phone/create.php">Add Phone+</a>
  <thead>
    <tr>  
      <th scope="col">Students name</th>
      <th scope="col">Teachers name</th>
      <th scope="col">Phone number</th>
      <th scope="col ">Buttons</th>
    </tr>
  </thead>
  <tbody>
      <?php if(mysqli_num_rows($data) > 0) : ?>
      <?php while($phone= mysqli_fetch_assoc($data) ) : ?>
    <tr>
      <td scope="row"><?= $phone['name']?></td>
      <td><?= $phone['teachers_name'] ?></td>
      <td><?= $phone['phone_num'] ?></td>
      <td>
          <a type="button" class="btn btn-warning" href="/pages/students_phone/update.php?id=<?= $phone['id'] ?>">Update</a>
          <a type="button" class="btn btn-success" href="/pages/students_phone/show.php?id=<?= $phone['id'] ?>">Show</a>
          <a type="button" class="btn btn-danger" href="/core/students_phone/delete.php?id=<?= $phone['id'] ?>">Delete</a>
      </td>
    </tr>
    <?php endwhile ?>
    <?php endif ?>
  </tbody>
</table>