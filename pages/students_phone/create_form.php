<?php 
require_once($_SERVER['DOCUMENT_ROOT'].'/core/database.php');
$sql = "SELECT * FROM ustozlar";
$table_data = mysqli_query($conn, $sql);
if (mysqli_num_rows($table_data) > 0) {
    $ustozlar = [];
    while($ustoz = mysqli_fetch_assoc($table_data)) {
        $ustozlar[] = $ustoz;
    }
}

$sql2 = "SELECT * FROM students";
$table_data2 = mysqli_query($conn, $sql2);
if (mysqli_num_rows($table_data2) > 0) {
    $subjects = [];
    while($s = mysqli_fetch_assoc($table_data2)) {
        $students[] = $s;
    }
}

?>

<h1 class="my-4">Add Student Phone</h1>
<form action="/core/students_phone/store.php" method="POST">

  <div class="form-group mb-3">
        <label for="exampleFormControlSelect1">TEACHER Name :</label>
        <select name='teachers_name' class="form-control" id="exampleFormControlSelect1">
            <?php if (count($ustozlar) > 0) : ?>
                <?php foreach ($ustozlar as $ustoz) : ?>
                    <option value="<?= $ustoz['name'] ?>"><?= $ustoz['name']?></option>
                <?php endforeach; ?>
            <?php endif; ?>
        </select>
    </div>
  
    <div class="form-group mb-3">
        <label for="exampleFormControlSelect1">SELECT Student :</label>
        <select name='name' class="form-control" id="exampleFormControlSelect1">
            <?php if (count($students) > 0) : ?>
                <?php foreach ($students as $student) : ?>
                    <option value="<?= $student['name'] ?>"><?= $student['name']?></option>
                <?php endforeach; ?>
            <?php endif; ?>
        </select>
    </div>

    <div class="mb-3">
    <label class="form-label">Phone number</label>
    <input type="number" class="form-control" id="subject" name="phone_num">
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>