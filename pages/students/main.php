<?php
require_once ($_SERVER['DOCUMENT_ROOT'].'/core/database.php');
$sql = 'SELECT * FROM students';
$data = mysqli_query($conn,$sql);

?>
<h1>Students  Table</h1>
<table class="table ">
<a type="button" class="btn btn-primary mx-auto m-4 w-25" href="/pages/students/create.php">Add Student+</a>
  <thead>
    <tr>
      <th scope="col">Id</th>
      <th scope="col">Student Name</th>
      <th scope="col">Age</th>
      <th scope="col ">Buttons</th>
    </tr>
  </thead>
  <tbody>
      <?php if(mysqli_num_rows($data) > 0) : ?>
      <?php while($student = mysqli_fetch_assoc($data) ) : ?>
    <tr>
      <td scope="row"><?= $student['id']?></td>
      <td><?= $student['name'] ?></td>
      <td><?= $student['age'] ?></td>
      <td>
          <a type="button" class="btn btn-warning" href="/pages/students/update.php?id=<?= $student['id'] ?>">Update</a>
          <a type="button" class="btn btn-success" href="/pages/students/show.php?id=<?= $student['id'] ?>">Show</a>
          <a type="button" class="btn btn-danger" href="/core/students/delete.php?id=<?= $student['id'] ?>">Delete</a>
      </td>
    </tr>
    <?php endwhile ?>
    <?php endif ?>
  </tbody>
</table>