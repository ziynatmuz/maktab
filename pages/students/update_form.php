<?php
if(array_key_exists('id',$_GET) && !empty($_GET['id'])){
$id = $_GET['id'];
require_once ($_SERVER['DOCUMENT_ROOT'].'/core/database.php');
$sql = "SELECT * FROM students WHERE id=$id";
$status = mysqli_query($conn,$sql);
if(mysqli_num_rows($status) > 0){
    $student = mysqli_fetch_assoc($status);
}else{
    dump("$id li student yoq");
}
}

?>

<h1 class="my-4">Update Student</h1>
<form action="/core/students/update.php" method="POST">
  <div class="mb-3">
    <label for="name" class="form-label">Student name :</label>
    <input type="text" class="form-control" id="name" name="name" value="<?= $student['name'] ?>">
  </div>
  <div class="mb-3">
    <label  class="form-label">Age :</label>
    <input type="text" class="form-control"  name="age" value="<?= $student['age'] ?>">
    <input type="hidden" class="form-control"  name="id" value="<?= $student['id'] ?>">
  </div>
  
  <button type="submit" class="btn btn-primary">Update</button>
</form>