<h1 class="my-4">Create Student</h1>
<form action="/core/students/store.php" method="POST">
  <div class="mb-3">
    <label for="name" class="form-label">Name :</label>
    <input type="text" class="form-control" id="name" name="name">
  </div>
  <div class="mb-3">
    <label  class="form-label">Age :</label>
    <input type="number" class="form-control"  name="age">
  </div>
 
  <button type="submit" class="btn btn-primary">Submit</button>
</form>