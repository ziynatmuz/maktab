<h1 class="my-4">Create Class</h1>
<form action="/core/classes/store.php" method="POST">
  
  <div class="mb-3">
    <label class="form-label">Class Number :</label>
    <input type="number" class="form-control"  name="class_num">
  </div>
  <div class="mb-3">
    <label  class="form-label">Class Letter :</label>
    <input type="text" class="form-control"  name="class_letter">
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>