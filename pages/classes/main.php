<?php
require_once ($_SERVER['DOCUMENT_ROOT'].'/core/database.php');
$sql = 'SELECT * FROM classes';
$data = mysqli_query($conn,$sql);

?>
<h1>Classes Table</h1>
<table class="table ">
<a type="button" class="btn btn-primary mx-auto m-4 w-25" href="/pages/classes/create.php">Add Class+</a>
  <thead>
    <tr>
      <th scope="col">Id</th>
      <th scope="col">Class_number</th>
      <th scope="col">Class_letter</th>
      <th scope="col ">Buttons</th>
    </tr>
  </thead>
  <tbody>
      <?php if(mysqli_num_rows($data) > 0) : ?>
      <?php while($class = mysqli_fetch_assoc($data) ) : ?>
    <tr>
      <td scope="row"><?= $class['id']?></td>
      <td><?= $class['class_num'] ?></td>
      <td><?= $class['class_letter'] ?></td>
      <td>
          <a type="button" class="btn btn-warning" href="/pages/classes/update.php?id=<?= $class['id'] ?>">Update</a>
          <a type="button" class="btn btn-success" href="/pages/classes/show.php?id=<?= $class['id'] ?>">Show</a>
          <a type="button" class="btn btn-danger" href="/core/classes/delete.php?id=<?= $class['id'] ?>">Delete</a>
      </td>
    </tr>
    <?php endwhile ?>
    <?php endif ?>
  </tbody>
</table>