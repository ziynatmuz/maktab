<?php
require_once ($_SERVER['DOCUMENT_ROOT'].'/core/database.php');
$sql = 'SELECT * FROM ustozlar';
$data = mysqli_query($conn,$sql);

?>
<h1>Teachers Table</h1>
<table class="table ">
<a type="button" class="btn btn-primary mx-auto m-4 w-25" href="/pages/teachers/create.php">Add Teacher+</a>
  <thead>
    <tr>
      <th scope="col">Name</th>
      <th scope="col">Lastname</th>
      <th scope="col">Subject_Name</th>
      <th scope="col ">Buttons</th>
    </tr>
  </thead>
  <tbody>
      <?php if(mysqli_num_rows($data) > 0) : ?>
      <?php while($ustoz = mysqli_fetch_assoc($data) ) : ?>
    <tr>
      <td scope="row"><?= $ustoz['name']?></td>
      <td><?= $ustoz['lastname'] ?></td>
      <td><?= $ustoz['subject_name'] ?></td>
      <td>
          <a type="button" class="btn btn-warning" href="/pages/teachers/update.php?id=<?= $ustoz['id'] ?>">Update</a>
          <a type="button" class="btn btn-success" href="/pages/teachers/show.php?id=<?= $ustoz['id'] ?>">Show</a>
          <a type="button" class="btn btn-danger" href="/core/teachers/delete.php?id=<?= $ustoz['id'] ?>">Delete</a>
      </td>
    </tr>
    <?php endwhile ?>
    <?php endif ?>
  </tbody>
</table>