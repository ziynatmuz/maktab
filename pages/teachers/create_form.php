<?php
require_once ($_SERVER['DOCUMENT_ROOT'].'/core/database.php');
$sql2 = "SELECT * FROM subjects";
$table_data2 = mysqli_query($conn, $sql2);
if (mysqli_num_rows($table_data2) > 0) {
    $subjects = [];
    while($subject = mysqli_fetch_assoc($table_data2)) {
        $subjects[] = $subject;
    }
}

?>

<h1 class="my-4">Create Teacher</h1>
<form action="/core/teachers/store.php" method="POST">
  <div class="mb-3">
    <label for="name" class="form-label">Name :</label>
    <input type="text" class="form-control" id="name" name="name">
  </div>
  <div class="mb-3">
    <label for="lastname" class="form-label">Lastname :</label>
    <input type="text" class="form-control" id="lastname" name="lastname">
  </div>
  <div class="form-group mb-3">
        <label for="exampleFormControlSelect1">SELECT SUBJECT :</label>
        <select name='subject_name' class="form-control" id="exampleFormControlSelect1">
            <?php if (count($subjects) > 0) : ?>
                <?php foreach ($subjects as $subject) : ?>
                    <option value="<?= $subject['name'] ?>"><?= $subject['name']?></option>
                <?php endforeach; ?>
            <?php endif; ?>
        </select>
    </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>