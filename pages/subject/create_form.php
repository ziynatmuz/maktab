<?php 
require_once($_SERVER['DOCUMENT_ROOT'].'/core/database.php');
$sql = "SELECT * FROM ustozlar";
$table_data = mysqli_query($conn, $sql);
if (mysqli_num_rows($table_data) > 0) {
    $ustozlar = [];
    while($ustoz = mysqli_fetch_assoc($table_data)) {
        $ustozlar[] = $ustoz;
    }
}

?>

<h1 class="my-4">Create Subject</h1>
<form action="/core/subject/store.php" method="POST">
  
   <div class="form-group mb-3">
        <label for="exampleFormControlSelect1">CREATE SUBJECT :</label>
        <input type="text" class="form-control" name="name">
    </div>
 <div class="form-group mb-3">
        <label for="exampleFormControlSelect1">SELECT TEACHER :</label>
        <select name='teachers_id' class="form-control" id="exampleFormControlSelect1">
            <?php if (count($ustozlar) > 0) : ?>
                <?php foreach ($ustozlar as $ustoz) : ?>
                    <option value="<?= $ustoz['name'] ?>"><?= $ustoz['name']?></option>
                <?php endforeach; ?>
            <?php endif; ?>
        </select>
    </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>