<?php
require_once ($_SERVER['DOCUMENT_ROOT'].'/core/database.php');
$sql = 'SELECT * FROM subjects';
$data = mysqli_query($conn,$sql);

?>
<h1>Subjects Table</h1>
<table class="table ">
<a type="button" class="btn btn-primary mx-auto m-4 w-25" href="/pages/subject/create.php">Add Subject+</a>
  <thead>
    <tr>
      <th scope="col">Id</th>
      <th scope="col">subject_name</th>
      <th scope="col">Teachers name</th>
      <th scope="col ">Buttons</th>
    </tr>
  </thead>
  <tbody>
      <?php if(mysqli_num_rows($data) > 0) : ?>
      <?php while($subject = mysqli_fetch_assoc($data) ) : ?>
    <tr>
      <td scope="row"><?= $subject['id']?></td>
      <td><?= $subject['name'] ?></td>
      <td><?= $subject['teachers_id'] ?></td>
      <td>
          <a type="button" class="btn btn-warning" href="/pages/subject/update.php?id=<?= $subject['id'] ?>">Update</a>
          <a type="button" class="btn btn-success" href="/pages/subject/show.php?id=<?= $subject['id'] ?>">Show</a>
          <a type="button" class="btn btn-danger" href="/core/subject/delete.php?id=<?= $subject['id'] ?>">Delete</a>
      </td>
    </tr>
    <?php endwhile ?>
    <?php endif ?>
  </tbody>
</table>